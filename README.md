# Speedtest Server List

**Welcome!** This repository provides a curated list of Speedtest servers around the world. It includes server locations, names, and other useful details. 

**Big thanks to Pablo Alcayaga** for the script that generates this list!

**Server updates are automated** using a Jenkins process. This process runs a script to gather and update the server list once a month (due to the script's running time of about 9 hours). This ensures you always have current and accurate server information.

## Table of Contents

* [Introduction](#introduction)
* [Using the Server List](#usage)
* [Automation and Updates](#automation-and-updates)

## Introduction

This repository is your one-stop shop for a comprehensive Speedtest server list. It's perfect for:

  * Users who want to perform network speed tests using specific servers.
  * Developers building applications that require Speedtest server information.

## Using the Server List

There's no need for installation! Simply clone this repository to access the server list in various formats, including JSON, CSV, or plain text. Here's how to use them:

**Example: JSON format**

```json
[
  {
    "id": "1234",
    "name": "Server Name",
    "location": "City, Country",
    "url": "http://server.url/speedtest"
  },
  ...
]
```

**Example: CSV format**
```
id,name,location,url
1234,Server Name,City, Country,http://server.url/speedtest
...
```

## Automation and Updates

The Speedtest server list is produced by a Jenkins automation process. This process runs a script to gather and compile the complete list of servers.

## License
This project is licensed under the MIT License. See the LICENSE file for more details

